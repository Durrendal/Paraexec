## Paraagent Local Swank Testing

This dockerfile exists solely for testing the swank/tcp Paraagent listening service, it is not intended to be used as a listening agent that can be run on a server.

## Building:
In this directory run
```
docker build -t ptest .
```

Once built issue
```
docker run ptest
```

The default SSH port is 0.0.0.0:22
The default Swank Exec port is 127.0.0.1:12893

If you do not know the docker containers IP address you can find it with docker inspect <container-id> (which you can get from docker ps)