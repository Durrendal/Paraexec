LISP ?= sbcl
LISP_FLAGS ?= --no-userinit --non-interactive

QUICKLISP_URL = https://beta.quicklisp.org/quicklisp.lisp
DOWNLOAD_AGENT = curl
DOWNLOAD_AGENT_FLAGS = --output
QUICKLISP_DIR = quicklisp

all:
	$(MAKE) quicklisp
	$(MAKE) paraexec
	$(MAKE) paraagent
	$(MAKE) clean

quicklisp:
	mkdir -p $(QUICKLISP_DIR) ;\
	$(DOWNLOAD_AGENT) $(DOWNLOAD_AGENT_FLAGS) quicklisp.lisp $(QUICKLISP_URL)
	$(LISP) $(LISP_FLAGS) \
                --eval '(require "asdf")' \
                --load quicklisp.lisp \
                --eval '(quicklisp-quickstart:install :path "$(QUICKLISP_DIR)/")' \
                --eval '(uiop:quit)' \

	$(LISP) $(LISP_FLAGS) \
                --load ./quicklisp/setup.lisp \
                --eval '(require "asdf")' \
                --eval '(ql:update-dist "quicklisp" :prompt nil)' \
		--eval '(ql:quickload (list "asdf" "uiop" "lparallel" "unix-opts" "str" "swank-client" "swank" "usocket" "libssh2" "trivial-ssh"))' \
                --eval '(uiop:quit)'

paraexec:
	$(LISP) $(LISP_FLAGS) --eval '(require "asdf")' \
		--load ./src/paraexec.asd \
		--load ./quicklisp/setup.lisp \
		--eval '(ql:quickload :paraexec)' \
		--eval '(ql:quickload (list "uiop" "lparallel" "unix-opts" "str" "swank-client" "usocket"))' \
		--eval '(asdf:make :paraexec)' \
		--eval '(quit)'

paraagent:
	$(LISP) $(LISP_FLAGS) --eval '(require "asdf")' \
		--load ./src/paraagent.asd \
		--load ./quicklisp/setup.lisp \
		--eval '(ql:quickload :paraagent)' \
		--eval '(ql:quickload (list "uiop" "unix-opts" "swank" "usocket"))' \
		--eval '(asdf:make :paraagent)' \
		--eval '(quit)'
clean:
	rm quicklisp.lisp ;\
	rm -rf $(QUICKLISP_DIR) ;\
