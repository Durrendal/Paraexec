# Paraexec:

## What:

Paraexec is an asychronous execution program. It takes a list of target systems defined by their hostname or lan/wan/vpn ips, and executes a script against them. Paraexec operates by opening a thread up to the maximum number of available CPU threads, and dispatching workers to run the script against each defined host.

## Usage:

### Create a host file:

```
(in-package paraexec)

(defparameter *hosts* (list "192.168.0.10" "192.168.0.11" "Server1" "Server2"))
```

A host configuration file needs to have an in-package argument directed at paraexec, and it must contain a parameterized list of hosts. The hosts themselves can be defined as IP addresses, or hostnames. For paraexec to function you must utilize ssh keys, and to operate off of hostnames, you need to have the servers configured in your .ssh/config or via DNS.

### Create a script file:

```
(in-package paraexec)

(defparameter *script* "printf '===[' && cat /etc/hostname | sed -e 's/$/]===/' ;\
	       apk update ;\
	       apk upgrade ;\
	       docker pull durrendal/mrbuildit:latest ;\
	       rc-status ;\
	       rc-update add docker ;\
	       reboot")

;; Script Commentary:
;; Create a ===[hostname]=== tag
;; Update Alpine APK Repos
;; Upgrade Alpine APK Packages
;; Pull Mrbuildit:latest docker file from durrendal's dockerhub
;; Check the Openrc service status
;; Add docker to run at next system reboot
;; Reboot the target system
```

Just like the host file we require an in-package arg, followed by a parameterized script, however in this instance the value of script is a simple string containing the contents of our script. Comments inside of the *script* body are handled with the standard lisp ;;, but should not be placed inside of the *script* body itself, as it will cause the script to fail.

The only caveat here is that regulars quotes currently break the script operations, so single quotes must be used inside of the *script* body.

If a function fails, it will not return the result, but will not prevent paraexec from operating.

```
(in-package paraexec)

(defparameter *script* "printf '===[' && cat /etc/hostname | sed -e 's/$/]===/' ;\
                        if [[ $USER != root ]]; then
                           echo 'User is not root, skipping'
                        else
                           apk update
                           apk upgrade
                         fi")
```

Script files can be more than simple series of commands to be run, full shell logic should be supported, though I have personally not yet tested anything outside of a simple if then statement.

```
(in-package paraexec)

(defparameter *script* "printf '===[' && cat /etc/hostname | sed -e 's/$/]===/' ;\                     
                       check_release () {
                                     grep ID /etc/*-release | sed '/VERSION/d' | cut -d= -f2
                        }

                        if [[ $(check_release) = 'alpine' ]]; then
                           apk update
                           apk upgrade
                        elif [[ $(check_release) = 'ubuntu' ]]; then
                           apt-get update
                           apt-get upgrade
                        elif [[ $(check_release) = 'debian' ]]; then
                           apt-get update
                           apt-get upgrade
                        elif [[ $($(check_release) | grep rhel) = 'rhel' ]]; then
                           yum upgrade
                        elif [[ $(check_release) = 'centos' ]]; then
                           yum upgrade
                        elif [[ $(check_release) = 'Arch Linux' ]]; then
                           pacman -Syy
                           pacman -Syu
                        fi")
```

Here's an example update/upgrade script that will trigger different package managers dependent upon host reported in the /etc/*-release files. This won't return anything currently if the system isn't connecting as root (bad design I know). In the nearish future I'll be looking into sshpass and credential management systems that can be applied to paraexec, much like ansibles.

There is a caveat here, when using the --supass argument scripts currently cannot be anything more than simple collections of commands, and they must be free of execessive white space. Our initial example wouldn't work unless we changed it to look like the script below.

```
(defparameter *script* "printf '===[' && cat /etc/hostname | sed -e 's/$/]===/' ;\
apk update ;\
apk upgrade ;\
docker pull durrendal/mrbuildit:latest ;\
rc-status ;\
rc-update add docker ;\
reboot")
```

This is because we currently scrape the script and transform each line in it to echo "$PASS" | sudo -p "" -u root -S $LINE, in roughly the same way as Ansible's sudo become module functions. I hope to continue development so that more complex script arguments can be properly passed with sudo exec.

### Running Paraexec:

Running paraexec is simple, it takes the following arguments:

```
Paraexec -- Author: Will Sinatra <will.sinatra@klockwork.net > <wpsinatra@gmail.com>

Usage: paraexec [-h] [-t|--targets ARG] [-s|--script ARG] [--sshpass ARG]
                [--supass ARG] [-d|--debug]

Available options:
  -h                       Copyright: GPLv3
  -t, --targets ARG        Path to List of Targets
  -s, --script ARG         Path to Script
  --sshpass ARG            Invoke with sshpass support, passing credentials as arg
  --supass ARG             Invoke sudo with provided password, allowing in script calls to be sudo executed.
  -d, --debug              Debug Paraexec

NIL
```

Example Output:

```
./paraexec -t Examples/readme-hosts -s Examples/readme-script -d

;;========== START DEBUG ==========;;
The hosts path is: /home/wsinatra/Development/WIP/paraexec/Examples/readme-hosts
The script path is: /home/wsinatra/Development/WIP/paraexec/Examples/readme-script
The new hosts: (cvm)
The new script: printf '===[' && cat /etc/hostname | sed -e 's/$/]===/' ;
               apk update ;
               apk upgrade
;;========== END DEBUG ==========;;

===[chronos-dev]===
fetch http://dl-3.alpinelinux.org/alpine/edge/main/x86_64/APKINDEX.tar.gz
fetch http://dl-3.alpinelinux.org/alpine/edge/community/x86_64/APKINDEX.tar.gz
fetch http://dl-3.alpinelinux.org/alpine/edge/testing/x86_64/APKINDEX.tar.gz
v20191114-615-ge0769baec3 [http://dl-3.alpinelinux.org/alpine/edge/main]
v20191114-630-g04ce325809 [http://dl-3.alpinelinux.org/alpine/edge/community]
v20191114-627-g12e0a8d301 [http://dl-3.alpinelinux.org/alpine/edge/testing]
OK: 15185 distinct packages available
(1/31) Upgrading busybox (1.31.1-r0 -> 1.31.1-r1)
Executing busybox-1.31.1-r1.post-upgrade
(2/31) Upgrading alpine-conf (3.8.3-r0 -> 3.8.3-r1)
(3/31) Upgrading ssl_client (1.31.1-r0 -> 1.31.1-r1)
(4/31) Upgrading busybox-suid (1.31.1-r0 -> 1.31.1-r1)
(5/31) Upgrading rust-stdlib (1.38.0-r2 -> 1.39.0-r0)
(6/31) Upgrading rust (1.38.0-r2 -> 1.39.0-r0)
(7/31) Purging llvm-libunwind-dev (9.0.0-r0)
(8/31) Purging llvm-libunwind (9.0.0-r0)
(9/31) Purging llvm-libunwind-static (9.0.0-r0)
(10/31) Upgrading cargo (1.38.0-r2 -> 1.39.0-r0)
(11/31) Upgrading containerd (1.3.0-r0 -> 1.3.1-r0)
(12/31) Upgrading libtasn1 (4.14-r0 -> 4.15.0-r0)
(13/31) Upgrading glib (2.62.2-r0 -> 2.62.3-r0)
(14/31) Upgrading imagemagick-libs (7.0.8.64-r1 -> 7.0.9.2-r0)
(15/31) Upgrading avahi-libs (0.7-r3 -> 0.7-r4)
(16/31) Upgrading imagemagick (7.0.8.64-r1 -> 7.0.9.2-r0)
(17/31) Upgrading linux-vanilla (4.19.84-r0 -> 4.19.85-r0)
(18/31) Upgrading mesa (19.2.4-r0 -> 19.2.5-r0)
(19/31) Upgrading mesa-gbm (19.2.4-r0 -> 19.2.5-r0)
(20/31) Upgrading mesa-glapi (19.2.4-r0 -> 19.2.5-r0)
(21/31) Upgrading mesa-egl (19.2.4-r0 -> 19.2.5-r0)
(22/31) Upgrading mesa-gl (19.2.4-r0 -> 19.2.5-r0)
(23/31) Upgrading mesa-gles (19.2.4-r0 -> 19.2.5-r0)
(24/31) Upgrading mesa-osmesa (19.2.4-r0 -> 19.2.5-r0)
(25/31) Upgrading mesa-xatracker (19.2.4-r0 -> 19.2.5-r0)
(26/31) Upgrading mesa-dev (19.2.4-r0 -> 19.2.5-r0)
(27/31) Upgrading sdl (1.2.15-r11 -> 1.2.15-r12)
(28/31) Upgrading strace (5.3-r0 -> 5.3-r1)
(29/31) Upgrading xorg-server (1.20.5-r0 -> 1.20.6-r0)
(30/31) Upgrading xterm (350-r0 -> 351-r0)
(31/31) Upgrading xvfb (1.20.5-r0 -> 1.20.6-r0)
Executing busybox-1.31.1-r1.trigger
Executing kmod-26-r0.trigger
Executing mkinitfs-3.4.3-r2.trigger
==> initramfs: creating /boot/initramfs-vanilla
Executing syslinux-6.04_pre1-r4.trigger
OK: 2164 MiB in 386 packages
NIL

```

NOTE:
You will not see output until a thread has stopped running!
Targets that cannot be reached will return NIL

# Building:

Depends on:
(sbcl nproc)

Running ./build.sh or make from the root directory will compile paraexec in ./src. Make is the prefered compilation method.

```
Verified Builds:

SBCL 2.0.1 | Alpine Linux | x86_64
SBCL 2.0.0 | Arch Linux | x86_64
SBCL 1.3.14 | Debian 9 | x86_64

Packaged:

Alpine Linux | Edge | Testing
```

# License:

Paraexec is licensed under GPLv3