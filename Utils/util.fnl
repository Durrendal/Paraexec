;; Author: Will Sinatra <wpsinatra@gmail.com>
;; Copyright: GPLv3
;; This code requires fennel to run! Further information about fennel can be found here: https://fennel-lang.org/

;; Generate paraexec script files
;; View snippets in this repo for an optional tagline option
;; (gen-parascript ["uname -a" "ip addr" "apk update" "apk upgrade"])
(global gen-parascript
        (fn [tbl]
            (let
                [dec "(in-package paraexec)"
                 newline "\n"
                 param "(defparameter *script* "
                 opsep " ;"
                 dblq "\""
                 tail ")"]
              (global final (.. dec newline newline param dblq))
              (each [key val (ipairs tbl)]
                    (let [opstring final]
                      (global final
                              (.. opstring val opsep newline))))
              (print (.. final dblq tail)))))

;; Generate paraexec host files
;; (gen-parahosts ["192.168.0.10" "10.4.0.1" "ssh-alias" "ssh-alias2"])
(global gen-parahosts
        (fn [tbl]
            (let
                [dec "(in-package paraexec)"
                 newline "\n"
                 param "(defparameter *hosts* (list"
                 dblq  "\""
                 space " "
                 tail "))"]
              (global final (.. dec newline newline param))
              (each [key val (ipairs tbl)]
                    (let [opstring final]
                      (global final
                              (.. opstring space dblq val dblq newline))))
              (print (.. final tail)))))
