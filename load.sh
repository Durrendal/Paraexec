#!/bin/bash
#This is simply meant for testing, it does not replace a compiled paraexec binary
builddir=./src/

load(){
    cd $builddir

    sbcl --eval '(ql:quickload (list "asdf" "uiop" "lparallel" "unix-opts" "str"))' \
         --eval '(asdf:operate (quote asdf:load-op) (quote paraexec))' \
	 --eval '(in-package paraexec)' \
	 --eval '(load "~/ParaData/scripts/test")' \
	 --eval '(load "~/ParaData/hosts/test")'
}

load
