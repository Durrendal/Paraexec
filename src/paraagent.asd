(defpackage #:paraagent
  (:use :cl :asdf))

#+sb-core-compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression t))

(asdf:defsystem #:paraagent
		:description "Paraexec Swank Agent"
		:author "Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>"
		:license "GPLv3"
		#+asdf-unicode :encoding #+asdf-unicode :utf-8
		:serial t
		:build-operation "program-op"
		:build-pathname "paraagent"
		:entry-point "paraagent:main"
		:depends-on (#:uiop
			     #:unix-opts
			     #:swank
			     #:usocket)
		:components ((:file "paraagent")))
