;;CopyrightL GPLv3

;;Author: Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>

;;Paraexec lisp execution agent
(uiop:define-package #:paraagent
    (:use :cl :uiop :swank :usocket)
  (:export :main))

(in-package paraagent)

;;===== Variables =====
(defvar *sport* 12893)
(defvar *addr* "0.0.0.0")
(defvar *port* 12893)
(defvar *state* "RUNNING")
(defvar *tors* "t")

;;===== Embedded Commands =====
(defun urunner (cmd)
  (format t "this is urunner, we got ~a~%" cmd)
  (with-output-to-string (out)
    (uiop:run-program cmd :output out))
  (format t "If you see this, uiop ran~%"))

(defun eval-and-exec (cmd)
  (if (eql cmd "ls")
      (print "we got an ls")
      (progn
	(print cmd)
	(print "wtf?"))))

;;===== Listen/Swank Server =====
;;https://gist.github.com/shortsightedsid/71cf34282dfae0dd2528
(defun tcp-listen ()
  (usocket:with-socket-listener
      (socket *addr* *port*)
    (loop
	 (usocket:with-connected-socket (connection (usocket:socket-accept socket))
	   (let
	       ((rec (read-line (usocket:socket-stream connection))))
	     (print rec)
	     (format t "~a~%"
		     (with-output-to-string (out)
		       (uiop:run-program "ls" :output out)))
	     (format (usocket:socket-stream connection) (with-output-to-string (out)
				  (uiop:run-program "ls" :output out))))))))

(defun start-agent (agenttyp)
  (if (eql agenttyp "swank")
      (progn
	(format t "[INFO]  Starting Swank Server..~%")
	(swank:create-server :port *sport* :dont-close t))
      (progn
	(format t "[INFO]  Starting TCP Listener ~a:~a..~%" *addr* *port*)
	(tcp-listen))))

(defun stop-agent (agenttyp)
  (if (eql agenttyp "swank")
      (progn
	(format t "[INFO]  Stopping Swank Server..~%")
	(swank:stop-server *sport*)
	(format t "[INFO]  Swank Server Stopped..~%"))
      (progn
	(format t "[INFO]  Stopping TCP Listener ~a:~a..~%" *addr* *port*)
	(tcp-listen))))

(defun agent-loop (agenttyp)
  (start-agent agenttyp)
  (loop until (eql *state* "STOPPED")
	do (progn
	     ;;system manager hooks
	     (sleep 90)))
  (stop-agent agenttyp))

(opts:define-opts
 (:name :help
	:description "Helpful help messages"
	:short #\h)
 (:name :swank
	:description "Run Paraagent in Swank mode, set swank port to ARG"
	:long "swank"
	:short #\s
	:arg-parser #'identity)
 (:name :addr
	:description "Paraagent host address, defaults to 0.0.0.0"
	:long "addr"
	:short #\a
	:arg-parser #'identity)
 (:name :port
	:description "Paraagent listening port, defaults to 12893"
	:long "port"
	:short #\p
	:arg-parser #'identity))

(defun unknown-option (condition)
  "If option is unknown"
  (format t "[WARN]  ~s is unknown!~%" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defmacro when-option ((options opt) &body body)
  "When option is called, do"
  `(let ((it (getf ,options ,opt)))
	 (when it
	   ,@body)))

(defun main ()
  "Init paraagent with defined port"
  (multiple-value-bind (options)
      (handler-case
	  (handler-bind ((opts:unknown-option #'unknown-option))
	    (opts:get-opts))
	(opts:missing-arg (condition)
	  (format t "[ERR]  Option ~s needs an argument~%"
		  (opts:option condition)))
	(opts:arg-parser-failed (condition)
	  (format t "[ERR]  Cannot parse ~s as argument of ~s~%"
		  (opts:raw-arg condition)
		  (opts:option condition)))
	(opts:missing-required-option (con)
	  (format t "[ERR]  ~a~%" con)
	  (opts:exit 1)))
    (when-option (options :help)
		 (opts:describe
		  :prefix "Paraagent -- Author: Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>"
		  :usage-of "paraagent")
		 (sb-ext:quit))
    (when-option (options :addr)
		 (setf *addr* (getf options :addr)))
    (when-option (options :port)
		 (setf *port* (parse-integer (getf options :port))))
    (when-option (options :swank)
		 (setf *sport* (parse-integer (getf options :swank)))
		 (setf *tors* "s"))
    (if (eql *tors* "s")
	(agent-loop "swank")
	(agent-loop "tcp"))))
