(defpackage #:paraexec
  (:use :cl :asdf))

#+sb-core-compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression t))

(asdf:defsystem #:paraexec
		:description "Parallel Exec"
		:author "Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>"
		:license "GPLv3"
		#+asdf-unicode :encoding #+asdf-unicode :utf-8
		:serial t
		;;Currently broken: pending CFFI upgrade in QL repo
		;;:defsystem-depends-on (:deploy)
		;;:build-operation "deploy-op"
		:build-operation "program-op"
		:build-pathname "paraexec"
		:entry-point "paraexec:main"
		:depends-on (#:uiop
			     #:lparallel
			     #:unix-opts
			     #:str
			     #:swank-client
			     #:usocket)
		:components ((:file "paraexec")))
