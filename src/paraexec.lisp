;;Copright: GPLv3

;;Author: Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>

;;Paraexec async executor
(uiop:define-package #:paraexec
		     (:use :cl :uiop :lparallel :swank-client)
		     (:export :main))

(in-package paraexec)

;;===== Define Thread Handling =====
(defparameter *threads* 0)

(defun kinit ()
  (setf lparallel:*kernel* (lparallel:make-kernel *threads* :name "exec-channel")))

(defun kdeinit ()
  (lparallel:end-kernel :wait t))

;;===== Variables =====
(defparameter *hosts* NIL)
(defvar *hpath* NIL)

(defparameter *script* NIL)
(defparameter *sudoscript* NIL)
(defparameter *suscript* NIL)
(defvar *spath* NIL)

(defvar *normexec* "T")
(defvar *sshpass* NIL)
(defvar *sudoexec* NIL)
(defvar *suexec* NIL)
(defvar *swankexec* NIL)

(defvar *sshcred* NIL)
(defvar *supass* NIL)
(defvar *sudopass* NIL)

(defvar *state* "RUNNING")

(defvar *swankclients* 0)

;;===== Functions =====
(defun paraexec-debug ()
  (format t ";;========== START DEBUG ==========;;~%")
  (format t "Available Threads: ~a~%" *threads*)
  (format t "Sshpass is enabled: ~a~%" *sshpass*)
  (format t "Suexec is enabled: ~a~%" *suexec*)
  (format t "Sudoexec is enabled: ~a~%" *sudoexec*)
  (format t "Target path: ~a~%" (probe-file *hpath*))
  (format t "Script path: ~a~%" (probe-file *spath*))
  (format t "Targets: ~a~%" *hosts*)
  (format t "Script: ~a~%" *script*)
  (format t ";;========== END DEBUG ==========;;~%~%"))

(defun define-threads ()
  (parse-integer
   (with-output-to-string (outstream)
			  (uiop:run-program "nproc" :output outstream))))

(defun file-string (path)
  "Ingest file as string"
  (with-open-file (stream path)
		  (let
		      ((data (make-string (file-length stream))))
		    (read-sequence data stream)
		    data)))

(defun nihil (supress)
  "Call function, supress output"
  (with-output-to-string
    (*standard-output*
     (make-array '(0) :element-type 'base-char :fill-pointer 0 :adjustable t))
    (funcall supress)
    (values)))

(defun errp ()
  "Positive Error"
  (pprint "This check returned no errors"))

(defun errn ()
  "Negative Error"
  (pprint "This check returned errors"))

(defmacro exists? (check string &body args)
  "Does this exist, if NIL do not return, if T return (T X) where X is the position of the occurence"
  `(let
       ((lint (format NIL "~a" (search ,check ,string ,@args))))
     (if (equal lint "NIL")
	 (nihil #'errp)
       (progn
	 (format t "Illegal call at position ~a in ~%~a~%" lint ,string)
	 (sb-ext:quit)))))

(defun cfg-lint (path)
  "Check Hosts/Script conf for illegal lisp calls"
  (exists? "(defun" (file-string path))
  (exists? "(defmacro" (file-string path))
  (exists? "(lambda" (file-string path))
  (exists? "(funcall" (file-string path))
  (exists? "(defvar" (file-string path))
  (exists? "(let" (file-string path))
  (exists? "(loop" (file-string path))
  (exists? "(dotimes" (file-string path))
  (exists? "(progn" (file-string path))
  (exists? "(when" (file-string path))
  (exists? "(if" (file-string path))
  (exists? "(with-open-file" (file-string path))
  (exists? "(intern" (file-string path))
  (exists? "(export" (file-string path))
  (exists? "(setf" (file-string path))
  (exists? "(setq" (file-string path))
  (exists? "(dolist" (file-string path))
  (exists? "(remove" (file-string path))
  (exists? "(replace" (file-string path))
  (exists? "(substitute" (file-string path))
  (exists? "(ql:quickload" (file-string path))
  (exists? "(read" (file-string path))
  (exists? "(quit" (file-string path))
  (exists? "(sb-ext:" (file-string path)))

(defun string-strip (string)
  "Strip the string of things that make su exec fail"
  (str:replace-all "\"echo" "echo"
		   (str:replace-all "chit\"" ""
				    (str:replace-all "\\" "" string))))

(defun sudo-script ()
  "Wrap all lines in *script* with a sudo exec line"
  (let
      ((lines (str:lines *script*)))
    (loop for l in lines
	  collect (concatenate 'string "echo " "\"" *sudopass* "\"" " | sudo -p " "\"" "\"" " -u root -S " l) into changed
       finally (setf *sudoscript* (format NIL "~{~A~}" changed)))))

(defun su-script ()
  "Append sudo su before script"
  (let
      ((suscript (write-to-string (concatenate 'string "echo " "\"" *supass* "\"" " | sudo -p " "\"" "\"" " -S su -c " "\"" *script* "\"" "chit"))))
    (setf *suscript* suscript)))

(defun ssh-wrap (host script) ;;&optional user? ;;Fails silently if sshpass isn't installed but called
  "Wrap script args in ssh -t formatting"
  (if (equal *sshpass* "T")
      (concatenate 'string "sshpass -p " *sshcred* " ssh -t -o ConnectTimeout=60 -o StrictHostKeyChecking=no " host " '" script "'")
      ;; This kludge is so nasty, but it somehow works..
	(string-strip (concatenate 'string "ssh -t -o ConnectTimeout=60 -o StrictHostKeyChecking=no " host " '" script "'"))))

(defun sshL-wrap (host port)
  "Forward swank host across SSH port"
  (concatenate 'string "ssh -L" port ":127.0.0.1:" port " " host))

;;Manual Swank Exec Example
;;(swank-client:with-slime-connection (connection "127.0.0.1" 12893)
;;(swank-client:slime-eval '(with-output-to-string (out) (uiop:run-program "apk update" :output out)) connection)

(defun swankexec (host port script)
  "Wrap script code in swank connect/execute body"
  (swank-client:with-slime-connection (connection host port)
    (swank-client:slime-eval (quote script) connection)))

(defun exec-across (host)
  "Execute ssh-wrapped script against target"
  (cond
    ((equal *sudoexec* "T")
     (uiop:run-program (ssh-wrap host *sudoscript*) :output t :ignore-error-status t))
    ((equal *suexec* "T")
     (uiop:run-program (ssh-wrap host *suscript*) :output t :ignore-error-status t))
    ((equal *normexec* "T")
     (uiop:run-program (ssh-wrap host *script*) :output t :ignore-error-status t))
    ((equal *swankexec* "T")
     (uiop:run-program (sshL-wrap host port) :output t :ignore-error-status t)
     (swankexec host port script))))

(defun paraexec ()
  "Asynchronously execute *script* against all defined *hosts*, report output to *standard-output*"
  (let*
      ((channel (lparallel:make-channel))
       (results nil)
       (ophosts *hosts*))
    (loop while (eql *state* "RUNNING") do
	 (dotimes (n (length *hosts*))
	   (if (eql *state* "STOPPED")
	       (return)
	       (let
		   ((host (pop ophosts)))
		 (lparallel:submit-task channel #'exec-across host)
		 (format t "~a~%~%" (lparallel:receive-result channel)))))
	 (setf *state* "STOPPED"))))

;;===== Unix Opts & Signals =====
;;===== with-user-abort start =====
;;https://github.com/compufox/with-user-abort
;;License: BSD 3-Clause Author: Compufox
(define-condition user-abort (#+sbcl sb-sys:interactive-interrupt)
  ()
  (:documentation "condition that inherits from implementation specific interrupt condition"))

(defun user-abort (&optional condition)
  (declare (ignore condition))
  (signal 'user-abort))

(defmacro with-user-abort (&body body)
  "execute BODY, signalling user-abort if the interrupt signal is received"
  `(handler-bind ((#+sbcl sb-sys:interactive-interrupt
		    #'user-abort))
     ,@body))
;;===== with-user-abort end =====

(defun abort-paraexec ()
  (format t "~%[WARN]  Aborting Paraexec..~%")
  (setf *state* "STOPPED")
  (lparallel:kill-tasks :default)
  ;;Not waiting for the workers to report might cause a race condition if the kill-task does not catch all works
  (lparallel:end-kernel :wait NIL)
  (uiop:quit 1))

(opts:define-opts
 (:name :help
	:description "Helpful help messages"
	:short #\h)
 (:name :hosts
	:description "Path to List of Targets"
	:short #\t
	:long "targets"
	:arg-parser #'identity)
 (:name :script
	:description "Path to Script"
	:short #\s
	:long "script"
	:arg-parser #'identity)
 (:name :ssh
	:description "Invoke with sshpass support with provided credentials"
	:long "ssh"
	:arg-parser #'identity)
 (:name :w/su
	:description "Invoke sudo su with provided credentials."
	:long "w/su"
	:arg-parser #'identity)
 (:name :sudo
	:description "Invoke all script lines with sudo, with provided credentials."
	:long "sudo"
	:arg-parser #'identity)
 (:name :swank
	:description "Invoke with Swank against Paraagent on target"
	:long "swank")
 (:name :debug
	:description "Debug Paraexec"
	:short #\d
	:long "debug"))

(defun unknown-option (condition)
  "If option is unknown"
  (format t "[WARN]  ~s is unknown!~%" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defmacro when-option ((options opt) &body body)
  "When option is called, do"
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defun main ()
  "Init thread kernel, ingest args, paraexec, deinit kernel"
  (multiple-value-bind (options)
		       (handler-case
			   (handler-bind ((opts:unknown-option #'unknown-option))
			     (opts:get-opts))
			 (opts:missing-arg (condition)
					   (format t "[ERR]  Option ~s needs an argument~%"
						   (opts:option condition)))
			 (opts:arg-parser-failed (condition)
						 (format t "[ERR]  Cannot parse ~s as argument of ~s~%"
							 (opts:raw-arg condition)
							 (opts:option condition)))
			 (opts:missing-required-option (con)
						      (format t "[ERR]  ~a~%" con)
						      (opts:exit 1)))
		       (when-option (options :help)
				    (opts:describe
				     :prefix "Paraexec -- Author: Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>"
				     :usage-of "paraexec"))
		       (when-option (options :hosts)
				    (setf *hpath* (getf options :hosts))
				    (cfg-lint *hpath*)
				    (load (getf options :hosts)))
		       (when-option (options :script)
				    (setf *spath* (getf options :script))
				    (cfg-lint *spath*)
				    (load (getf options :script)))
		       (when-option (options :ssh)
				    (setf *sshcred* (getf options :ssh))
				    (setf *sshpass* "T"))
		       (when-option (options :sudo)
				    (setf *sudopass* (getf options :sudo))
				    (setf *sudoexec* "T")
				    (setf *normexec* NIL)
				    (sudo-script))
		       (when-option (options :w/su)
				    (setf *supass* (getf options :w/su))
				    (setf *suexec* "T")
				    (setf *normexec* NIL)
				    (su-script))
		       (when-option (options :swank)
				    (setf *swankexec "T")
				    (setf *normexec* NIL))
		       (when-option (options :debug)
				    (setf *threads* (define-threads))
				    (paraexec-debug))
		       (handler-case
			   (with-user-abort (progn
					      (setf *threads* (define-threads))
					      (kinit)
					      (paraexec)
					      (kdeinit)))
			 (user-abort ()
				     (abort-paraexec)))))
