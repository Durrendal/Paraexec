(uiop:define-package #:test
    (:use :cl :usocket)
  (:export :help
	   :tcp-throw
	   :server
	   :client))

(in-package test)

(defun help ()
  (format t "(tcp-throw addr port string)~%"))

(defun tcp-throw (addr port data)
  (with-client-socket
      (socket stream addr port)
    (format stream data)
    (force-output stream)))

;;https://lispcookbook.github.io/cl-cookbook/sockets.html
;;In this example the client conencts to the server, and the server pushes data down to the client
;;Pretty standard for something like a config manager, connect to server, request my specific config, get sent data, stop
;;We need to just reverse the layout, but functionally it's the same thing labelled differently.
(defun server (port msg)
  (let* ((socket (usocket:socket-listen "127.0.0.1" port))
	 (connection (usocket:socket-accept socket :element-type 'character)))
    (unwind-protect
	 (progn
	   (format (usocket:socket-stream connection) (concatenate 'string msg "~%"))
	   (force-output (usocket:socket-stream connection)))
      (progn
	(format t "Closing sockets~%")
	(usocket:socket-close connection)
	(usocket:socket-close socket)))))

(defun client (port)
  (usocket:with-client-socket (socket stream "127.0.0.1" port :element-type 'character)
    (unwind-protect
	 (progn
	   (usocket:wait-for-input socket)
	   (format t "Input is: ~a~%" (read-line stream)))
      (usocket:socket-close socket))))
